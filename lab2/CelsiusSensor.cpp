#include <cstdlib>
#include <ctime>

#include "CelsiusSensor.h"

float CelsiusSensor::getCelsiusTemperature() const {
	srand(time(NULL));
	//float temperature = rand() % 201 - 100; // -100°C ... +100°C;

	// -100°F (-148°F) ... +100°C (+212°F);
	float temperature = round( (((rand() % 361 - 148) - 32.0) * 5.0 / 9.0) * 100) / 100; 
	return temperature;
}