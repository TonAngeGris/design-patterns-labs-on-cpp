#pragma once

class ClimateControlSystem {
public:
	virtual float getTemperature() const = 0;

	virtual ~ClimateControlSystem() {};
};