#include <iostream>

#include "Interface.h"

int main() {
	setlocale(LC_ALL, "Russian");

	Interface runner;
	runner.run();

	return 0;
}