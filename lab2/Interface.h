#pragma once

class Interface {
public:
	void run();

private:
	void commandList() const;
};