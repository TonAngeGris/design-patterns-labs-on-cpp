#include <cstdlib>
#include <ctime>

#include "FahrenheitSensor.h"

float FahrenheitSensor::getTemperature() const {
	srand(time(NULL));
	float temperature = rand() % 361 - 148; // -148°F (-100°C) ... +212°F (+100°C);
	return temperature;
}