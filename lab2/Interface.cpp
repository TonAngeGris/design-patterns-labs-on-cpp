#include <iostream>

#include "Interface.h"
#include "FahrenheitSensor.h"
#include "CelsiusSensor.h"
#include "Adapter.h"

using std::cout;
using std::cin;
using std::endl;

void Interface::run() {
	CelsiusSensor* cSensor = new CelsiusSensor();
	ClimateControlSystem* fSensor = new FahrenheitSensor();
	ClimateControlSystem* adapter = new Adapter(cSensor);

	int command = 1;
	commandList();
	while (command) {
		cin >> command;


		switch (command) {
		case 1:
			cout << fSensor->getTemperature() << "°F" << endl;
			break;

		case 2: 
			cout << adapter->getTemperature() << "°C" << endl;
			break;
		}
	}

	delete fSensor;
}

void Interface::commandList() const {
	cout <<
		"1. Вывести температуру в градусах Фаренгейта\n"
		"2. Вывести температуру в градусах Цельсия\n"
		"0. Выключить температурный датчик\n";
}