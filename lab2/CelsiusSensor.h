#pragma once

class CelsiusSensor {
public: 
	float getCelsiusTemperature() const;
};