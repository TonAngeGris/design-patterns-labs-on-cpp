#include "Adapter.h"
#include "CelsiusSensor.h"

float Adapter::getTemperature() const { 
	return _cSensor->getCelsiusTemperature(); 
}

Adapter::~Adapter() {
	delete _cSensor;
}
