#pragma once

#include "ClimateControlSystem.h"

class FahrenheitSensor : public ClimateControlSystem {
public:
	float getTemperature() const override;
};