#pragma once

#include "ClimateControlSystem.h"
#include "CelsiusSensor.h"

class Adapter : public ClimateControlSystem {
private:
	CelsiusSensor* _cSensor;

public:
	Adapter(CelsiusSensor* cSensor) : _cSensor(cSensor) {};

	float getTemperature() const override;

	~Adapter();
};