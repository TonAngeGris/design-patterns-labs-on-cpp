#include <iostream>

#include "Interface.h"
#include "FirstWallet.h"
#include "SecondWallet.h"
#include "ThirdWallet.h"

using std::cin;
using std::cout;
using std::endl;

void Interface::run() {
    FirstWallet* wallet1 = new FirstWallet;
    SecondWallet* wallet2 = new SecondWallet;
    ThirdWallet* wallet3 = new ThirdWallet;

    // Добавляем кошельки в цепочку
    wallet1->addWallet(wallet2);
    wallet2->addWallet(wallet3);
    wallet3->setLast();

    while (true) {
        int amount;
        cout << "Enter the amount to pay: $";
        cin >> amount;
        cout << endl;

        wallet1->pay(amount);
        cout << endl;
    }

    delete wallet1;
    delete wallet2;
    delete wallet3;
}