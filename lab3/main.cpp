/*
#include <iostream>

using namespace std;

class Wallet {
public:
    Wallet* next;

    Wallet() {
        next = 0;
    }

    // Установка конца цепочки
    void setLast() {
        next = 0;
    }

    // Добавление (связывание) кошельков
    void addWallet(Wallet* n) {
        if (next)
            next->addWallet(n);
        else
            next = n;

    }

    // Оплата или рекурсивный переход к следующему кошельку
    virtual void pay(int amount) {
        next->pay(amount);
    }
};

class Wallet1 : public Wallet {
public:
    void pay(int amount) {
        if (0 < amount && amount <= 20) {
            cout << "($" << amount << ") " << "The amount was paid from the 1st Wallet." << endl;
        }
        else {
            cout << "($" << amount << ") " << "There is not enough money in the 1st Wallet to pay this amount" << endl;

            if (next == 0) {
                cout << "The amount was not paid." << endl;
            }
            else {
                next->pay(amount);
            }
        }
        
    }
};

class Wallet2 : public Wallet {
public:
    void pay(int amount) {
        if (20 < amount && amount <= 50) {
            cout << "($" << amount << ") " << "The amount was paid from the 2nd Wallet." << endl;
        }
        else {
            cout << "($" << amount << ") " << "There is not enough money in the 2nd Wallet to pay this amount" << endl;

            if (next == 0) {
                cout << "The amount was not paid." << endl;
            }
            else {
                next->pay(amount);
            }
        }

    }
};

class Wallet3 : public Wallet {
public:
    void pay(int amount) {
        if (50 < amount && amount <= 100) {
            cout << "($" << amount << ") " << "The amount was paid from the 3rd Wallet." << endl;
        }
        else {
            cout << "($" << amount << ") " << "There is not enough money in the 3rd Wallet to pay this amount" << endl;

            if (next == 0) {
                cout << "The amount was not paid." << endl;
            }
            else {
                next->pay(amount);
            }
        }

    }
};

int main() {
    Wallet1 *w1 = new Wallet1;
    Wallet2 *w2 = new Wallet2;
    Wallet3 *w3 = new Wallet3;

    w1->addWallet(w2);
    w2->addWallet(w3);
    w3->setLast();

    int amount = 12;
    w1->pay(amount);

    delete w1;
    delete w2;
    delete w3;

    return 0;
}
    */

#include <iostream>

#include "Interface.h"

int main() {
    setlocale(LC_ALL, "Russian");

    Interface runner;
    runner.run();

    return 0;
}