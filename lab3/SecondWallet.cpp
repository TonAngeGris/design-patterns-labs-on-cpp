#include <iostream>

#include "SecondWallet.h"

using std::cout;
using std::endl;

void SecondWallet::pay(int amount) {
    if (20 < amount && amount <= 50) {
        cout << "($" << amount << ") " << "The amount was paid from the 2nd Wallet." << endl;
    }
    else {
        cout << "($" << amount << ") " << "There is not enough money in the 2nd Wallet to pay this amount" << endl;

        if (next == 0) {
            cout << "The amount was not paid." << endl;
        }
        else {
            next->pay(amount);
        }
    }
}