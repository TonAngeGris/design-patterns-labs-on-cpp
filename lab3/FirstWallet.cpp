#include <iostream>

#include "FirstWallet.h"

using std::cout;
using std::endl;

void FirstWallet::pay(int amount) {
    if (0 < amount && amount <= 20) {
        cout << "($" << amount << ") " << "The amount was paid from the 1st Wallet." << endl;
    }
    else {
        cout << "($" << amount << ") " << "There is not enough money in the 1st Wallet to pay this amount" << endl;

        if (next == 0) {
            cout << "The amount was not paid." << endl;
        }
        else {
            next->pay(amount);
        }
    }
}
