#pragma once

class Wallet {
public:
    Wallet* next;

    Wallet();

    // Установка конца цепочки
    void setLast();

    // Добавление (связывание) кошельков
    void addWallet(Wallet* n);

    // Оплата или рекурсивный переход к следующему кошельку
    virtual void pay(int amount);
};