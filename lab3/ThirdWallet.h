#pragma once

#include "Wallet.h"

class ThirdWallet : public Wallet {
public:
    void pay(int amount);
};