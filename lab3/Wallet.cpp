#include "Wallet.h"

Wallet::Wallet() {
    next = 0;
}

void Wallet::setLast() {
    next = 0;
}

void Wallet::addWallet(Wallet* n) {
    if (next)
        next->addWallet(n);
    else
        next = n;

}
     
void Wallet::pay(int amount) {
    next->pay(amount);
}
