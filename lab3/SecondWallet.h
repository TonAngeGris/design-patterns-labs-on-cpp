#pragma once

#include "Wallet.h"

class SecondWallet : public Wallet {
public:
    void pay(int amount);
};