#include <iostream>

#include "ThirdWallet.h"

using std::cout;
using std::endl;

void ThirdWallet::pay(int amount) {
    if (50 < amount && amount <= 100) {
        cout << "($" << amount << ") " << "The amount was paid from the 3rd Wallet." << endl;
    }
    else {
        cout << "($" << amount << ") " << "There is not enough money in the 3rd Wallet to pay this amount" << endl;

        if (next == 0) {
            cout << "The amount was not paid." << endl;
        }
        else {
            next->pay(amount);
        }
    }
}