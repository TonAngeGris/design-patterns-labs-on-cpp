#pragma once

#include "Wallet.h"

class FirstWallet : public Wallet {
public:
    void pay(int amount);
};