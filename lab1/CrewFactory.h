#pragma once

#include "AttributeFactory.h"

// Конкретная фабрика по обучению экипажа
class CrewFactory : public AttributeFactory {
public:
    CorvetteAttribute* createCorvetteAttribute(int number) override;

    DestroyerAttribute* createDestroyerAttribute(int number) override;

    CruiserAttribute* createCruiserAttribute(int number) override;
};