#pragma once

#include "CruiserAttribute.h"

// Конкретный класс, вооружение эсминца
class CruiserWeaponry : public CruiserAttribute {
public:
    CruiserWeaponry(int _number) : CruiserAttribute(_number) {};

    void info() const override;
};