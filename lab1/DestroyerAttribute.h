#pragma once

// јбстрактный класс, атрибут эсминца
class DestroyerAttribute {
protected:
    int _number;

public:
    DestroyerAttribute(int number) : _number(number) {};

    virtual void info() const = 0;
};