#include <iostream>

#include "CruiserCrew.h"

using std::cout;
using std::endl;

void CruiserCrew::info() const {
    cout << "Cruiser crew #" << _number << endl;
}