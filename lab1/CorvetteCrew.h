#pragma once

#include "CorvetteAttribute.h"

// Конкретный класс, экипаж корвета
class CorvetteCrew : public CorvetteAttribute {
public:
    CorvetteCrew(int _number) : CorvetteAttribute(_number) {};

    void info() const override;
};