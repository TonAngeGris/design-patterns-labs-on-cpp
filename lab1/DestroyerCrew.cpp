#include <iostream>

#include "DestroyerCrew.h"

using std::cout;
using std::endl;

void DestroyerCrew::info() const {
    cout << "Destroyer crew #" << _number << endl;
}