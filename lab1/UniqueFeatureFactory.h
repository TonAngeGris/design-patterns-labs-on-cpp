#pragma once

#include "AttributeFactory.h"

// Конкретная фабрика по уникальных особенных атрибутов
class UniqueFeatureFactory : public AttributeFactory {
public:
    CorvetteAttribute* createCorvetteAttribute(int number) override;

    DestroyerAttribute* createDestroyerAttribute(int number) override;

    CruiserAttribute* createCruiserAttribute(int number) override;
};
