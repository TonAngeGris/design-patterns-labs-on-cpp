#include <iostream>

#include "CorvetteWeaponry.h"

using std::cout;
using std::endl;

void CorvetteWeaponry::info() const {
    cout << "Corvette weaponry #" << _number << endl;
}
