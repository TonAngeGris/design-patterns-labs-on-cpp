#pragma once

#include "DestroyerAttribute.h"

// Конкретный класс, сила энергощитов эсминца (уникальная особенность)
class DestroyerUniqueFeature : public DestroyerAttribute {
public:
    DestroyerUniqueFeature(int _number) : DestroyerAttribute(_number) {};

    void info() const override;
};