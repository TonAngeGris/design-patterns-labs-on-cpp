#include "WeaponryFactory.h"
#include "CorvetteWeaponry.h"
#include "DestroyerWeaponry.h"
#include "CruiserWeaponry.h"

CorvetteAttribute* WeaponryFactory::createCorvetteAttribute(int number) {
    return new CorvetteWeaponry(number);
}

DestroyerAttribute* WeaponryFactory::createDestroyerAttribute(int number) {
    return new DestroyerWeaponry(number);
}

CruiserAttribute* WeaponryFactory::createCruiserAttribute(int number) {
    return new CruiserWeaponry(number);
}
