#include <iostream>

#include "CorvetteHull.h"

using std::cout;
using std::endl;

void CorvetteHull::info() const {
    cout << "Corvette hull #" << _number << endl;
}