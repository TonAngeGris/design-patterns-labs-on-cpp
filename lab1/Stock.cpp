#include <iostream>

#include "Stock.h"

using std::cout;
using std::cin;

void Stock::startFactory(HullFactory& factory) {
    int option = 0;
    cout << "Choose the option:\n"
        "1. Corvette Hull\n"
        "2. Destroyer Hull\n"
        "3. Cruiser Hull\n";
    cin >> option;

    switch (option) {
    case 1:
        corv_attr.push_back(factory.createCorvetteAttribute(++corvetteHullCounter));
        break;
    case 2:
        dest_attr.push_back(factory.createDestroyerAttribute(++destroyerHullCounter));
        break;
    case 3:
        cruis_attr.push_back(factory.createCruiserAttribute(++cruiserHullCounter));
        break;
    default:
        cout << "There's no such an option!";
        break;
    }
}

void Stock::startFactory(WeaponryFactory& factory) {
    int option = 0;
    cout << "Choose the option:\n"
        "1. Corvette Weaponry\n"
        "2. Destroyer Weaponry\n"
        "3. Cruiser Weaponry\n";
    cin >> option;

    switch (option) {
    case 1:
        corv_attr.push_back(factory.createCorvetteAttribute(++corvetteWeaponryCounter));
        break;
    case 2:
        dest_attr.push_back(factory.createDestroyerAttribute(++destroyerWeaponryCounter));
        break;
    case 3:
        cruis_attr.push_back(factory.createCruiserAttribute(++cruiserWeaponryCounter));
        break;
    default:
        cout << "There's no such an option!";
        break;
    }
}

void Stock::startFactory(CrewFactory& factory) {
    int option = 0;
    cout << "Choose the option:\n"
        "1. Corvette Crew\n"
        "2. Destroyer Crew\n"
        "3. Cruiser Crew\n";
    cin >> option;

    switch (option) {
    case 1:
        corv_attr.push_back(factory.createCorvetteAttribute(++corvetteCrewCounter));
        break;
    case 2:
        dest_attr.push_back(factory.createDestroyerAttribute(++destroyerCrewCounter));
        break;
    case 3:
        cruis_attr.push_back(factory.createCruiserAttribute(++cruiserCrewCounter));
        break;
    default:
        cout << "There's no such an option!";
        break;
    }
}

void Stock::startFactory(UniqueFeatureFactory& factory) {
    int option = 0;
    cout << "Choose the option:\n"
        "1. Corvette Unique Feature\n"
        "2. Destroyer Unique Feature\n"
        "3. Cruiser Unique Feature\n";
    cin >> option;

    switch (option) {
    case 1:
        corv_attr.push_back(factory.createCorvetteAttribute(++corvetteUniqueFeatureCounter));
        break;
    case 2:
        dest_attr.push_back(factory.createDestroyerAttribute(++destroyerUniqueFeatureCounter));
        break;
    case 3:
        cruis_attr.push_back(factory.createCruiserAttribute(++cruiserUniqueFeatureCounter));
        break;
    default:
        cout << "There's no such an option!";
        break;
    }
}

void Stock::info() const {
    unsigned int i;
    cout << "All stored objects:\n";
    for (i = 0; i < corv_attr.size(); ++i) corv_attr[i]->info();
    for (i = 0; i < dest_attr.size(); ++i) dest_attr[i]->info();
    for (i = 0; i < cruis_attr.size(); ++i) cruis_attr[i]->info();
}

Stock::~Stock() {
    unsigned int i;
    for (i = 0; i < corv_attr.size(); ++i) delete corv_attr[i];
    for (i = 0; i < dest_attr.size(); ++i) delete dest_attr[i];
    for (i = 0; i < cruis_attr.size(); ++i) delete cruis_attr[i]; 
}