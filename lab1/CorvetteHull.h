#pragma once

#include "CorvetteAttribute.h"

// Конкретный класс, корпус корвета
class CorvetteHull : public CorvetteAttribute {
public:
    CorvetteHull(int _number) : CorvetteAttribute(_number) {};

    void info() const override;
};