#pragma once

#include "DestroyerAttribute.h"

// Конкретный класс, экипаж эсминца
class DestroyerCrew : public DestroyerAttribute {
public:
    DestroyerCrew(int _number) : DestroyerAttribute(_number) {};

    void info() const override;
};