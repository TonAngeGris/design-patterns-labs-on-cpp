#include <iostream>

#include "DestroyerHull.h"

using std::cout;
using std::endl;

void DestroyerHull::info() const {
    cout << "Destroyer hull #" << _number << endl;
}