#include <iostream>

#include "DestroyerUniqueFeature.h"

using std::cout;
using std::endl;

void DestroyerUniqueFeature::info() const {
    cout << "Destroyer power shield #" << _number << endl;
}
