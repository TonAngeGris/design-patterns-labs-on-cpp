#include "HullFactory.h"
#include "CorvetteHull.h"
#include "DestroyerHull.h"
#include "CruiserHull.h"

CorvetteAttribute* HullFactory::createCorvetteAttribute(int number) {
    return new CorvetteHull(number);
}

DestroyerAttribute* HullFactory::createDestroyerAttribute(int number) {
    return new DestroyerHull(number);
}

CruiserAttribute* HullFactory::createCruiserAttribute(int number) {
    return new CruiserHull(number);
}