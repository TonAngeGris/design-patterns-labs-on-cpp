#include <iostream>

#include "CorvetteUniqueFeature.h"

using std::cout;
using std::endl;

void CorvetteUniqueFeature::info() const {
    cout << "Corvette engine #" << _number << endl;
}
