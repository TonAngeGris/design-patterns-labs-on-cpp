#pragma once

#include "DestroyerAttribute.h"

// Конкретный класс, вооружение эсминца
class DestroyerWeaponry : public DestroyerAttribute {
public:
    DestroyerWeaponry(int _number) : DestroyerAttribute(_number) {};

    void info() const override;
};