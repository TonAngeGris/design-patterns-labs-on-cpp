#pragma once

#include "CruiserAttribute.h"

// Конкретный класс, экипаж эсминца
class CruiserCrew : public CruiserAttribute {
public:
    CruiserCrew(int _number) : CruiserAttribute(_number) {};

    void info() const override;
};