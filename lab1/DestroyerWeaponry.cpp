#include <iostream>

#include "DestroyerWeaponry.h"

using std::cout;
using std::endl;

void DestroyerWeaponry::info() const {
    cout << "Destroyer weaponry #" << _number << endl;
}
