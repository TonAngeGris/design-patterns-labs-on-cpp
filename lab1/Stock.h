#pragma once

#include <vector>

#include "CorvetteAttribute.h"
#include "DestroyerAttribute.h"
#include "CruiserAttribute.h"
#include "HullFactory.h"
#include "WeaponryFactory.h"
#include "CrewFactory.h"
#include "UniqueFeatureFactory.h"

using std::vector;

//  ласс-хранилище созданных компонентов
class Stock {
private:
    // ’ранилище деталей всех семейств
    vector<CorvetteAttribute*> corv_attr;
    vector<DestroyerAttribute*> dest_attr;
    vector<CruiserAttribute*> cruis_attr;

    // —чЄтчик корпусов каждого типа
    int corvetteHullCounter = 0;
    int destroyerHullCounter = 0;
    int cruiserHullCounter = 0;

    // —чЄтчик вооружений каждого типа
    int corvetteWeaponryCounter = 0;
    int destroyerWeaponryCounter = 0;
    int cruiserWeaponryCounter = 0;

    // —чЄтчик экипажей каждого типа
    int corvetteCrewCounter = 0;
    int destroyerCrewCounter = 0;
    int cruiserCrewCounter = 0;

    // —чЄтчик уникальных особенностей каждого типа
    int corvetteUniqueFeatureCounter = 0;
    int destroyerUniqueFeatureCounter = 0;
    int cruiserUniqueFeatureCounter = 0;

public:
    void startFactory(HullFactory& factory);

    void startFactory(WeaponryFactory& factory);

    void startFactory(CrewFactory& factory);

    void startFactory(UniqueFeatureFactory& factory);

    void info() const;

    ~Stock();
};