#include <iostream>

#include "CruiserWeaponry.h"

using std::cout;
using std::endl;

void CruiserWeaponry::info() const {
    cout << "Cruiser weaponry #" << _number << endl;
}
