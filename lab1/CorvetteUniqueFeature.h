#pragma once

#include "CorvetteAttribute.h"

// Конкретный класс, продолжительность хода корвета (уникальная особенность)
class CorvetteUniqueFeature : public CorvetteAttribute {
public:
    CorvetteUniqueFeature(int _number) : CorvetteAttribute(_number) {};

    void info() const override;
};