#pragma once

// Ђбстрактный класс, атрибут корвета
class CorvetteAttribute {
protected:
    int _number;

public:
    CorvetteAttribute(int number) : _number(number) {};

    virtual void info() const = 0;
};