#pragma once

#include "CruiserAttribute.h"

// Конкретный класс, корпус эсминца
class CruiserHull : public CruiserAttribute {
public:
    CruiserHull(int _number) : CruiserAttribute(_number) {};

    void info() const override;
};