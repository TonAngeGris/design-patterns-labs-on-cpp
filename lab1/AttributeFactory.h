#pragma once

#include "CorvetteAttribute.h"
#include "DestroyerAttribute.h"
#include "CruiserAttribute.h"

// Абстрактная фабрика по созданию аттрибута
class AttributeFactory {
public:
    virtual CorvetteAttribute* createCorvetteAttribute(int number) = 0;
    virtual DestroyerAttribute* createDestroyerAttribute(int number) = 0;
    virtual CruiserAttribute* createCruiserAttribute(int number) = 0;

    ~AttributeFactory() {};
};