#pragma once

#include "CorvetteAttribute.h"

//  онкретный класс, вооружение корвета
class CorvetteWeaponry : public CorvetteAttribute {
public:
    CorvetteWeaponry(int _number) : CorvetteAttribute(_number) {};

    void info() const override;
};