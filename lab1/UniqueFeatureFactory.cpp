#include "UniqueFeatureFactory.h"
#include "CorvetteUniqueFeature.h"
#include "DestroyerUniqueFeature.h"
#include "CruiserUniqueFeature.h"

CorvetteAttribute* UniqueFeatureFactory::createCorvetteAttribute(int number) {
    return new CorvetteUniqueFeature(number);
}

DestroyerAttribute* UniqueFeatureFactory::createDestroyerAttribute(int number) {
    return new DestroyerUniqueFeature(number);
}

CruiserAttribute* UniqueFeatureFactory::createCruiserAttribute(int number) {
    return new CruiserUniqueFeature(number);
}