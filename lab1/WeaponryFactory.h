#pragma once

#include "AttributeFactory.h"

//  онкретна¤ фабрика по созданию вооружени¤
class WeaponryFactory : public AttributeFactory {
public:
    CorvetteAttribute* createCorvetteAttribute(int number) override;

    DestroyerAttribute* createDestroyerAttribute(int number) override;

    CruiserAttribute* createCruiserAttribute(int number) override;
};