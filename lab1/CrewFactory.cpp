#include "CrewFactory.h"
#include "CorvetteCrew.h"
#include "DestroyerCrew.h"
#include "CruiserCrew.h"

CorvetteAttribute* CrewFactory::createCorvetteAttribute(int number) {
    return new CorvetteCrew(number);
}

DestroyerAttribute* CrewFactory::createDestroyerAttribute(int number) {
    return new DestroyerCrew(number);
}

CruiserAttribute* CrewFactory::createCruiserAttribute(int number) {
    return new CruiserCrew(number);
}