#pragma once

// јбстрактный класс, атрибут эсминца
class CruiserAttribute {
protected:
    int _number;

public:
    CruiserAttribute(int number) : _number(number) {};

    virtual void info() const = 0;
};