#include <iostream>

#include "CruiserUniqueFeature.h"

using std::cout;
using std::endl;

void CruiserUniqueFeature::info() const {
    cout << "Cruiser accelerator #" << _number << endl;
}
