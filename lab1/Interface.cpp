#include <iostream>

#include "Interface.h"
#include "Stock.h"
#include "HullFactory.h"
#include "WeaponryFactory.h"
#include "CrewFactory.h"
#include "UniqueFeatureFactory.h"

using std::cout;
using std::cin;

void Interface::showOptions() const {
    cout << "\nPossible options:\n"
        "1. To launch a new Hull\n"
        "2. To launch a new Weapon\n"
        "3. To launch a new Crew\n"
        "4. To launch a new Unique Feature\n"
        "5. Show all stored objects\n"
        "9. Show options\n"
        "0. Exit program\n";
}

void Interface::run() {
    HullFactory hullFactory;
    WeaponryFactory weaponryFactory;
    CrewFactory crewFactory;
    UniqueFeatureFactory uniqueFeatureFactory;

    Stock* stock = new Stock;

    int option = 1;
    showOptions();

    while (option) {
        cin >> option;

        switch (option) {
        case 1:
            stock->startFactory(hullFactory);
            showOptions();
            break;
        case 2:
            stock->startFactory(weaponryFactory);
            showOptions();
            break;
        case 3:
            stock->startFactory(crewFactory);
            showOptions();
            break;
        case 4:
            stock->startFactory(uniqueFeatureFactory);
            showOptions();
            break;
        case 5:
            stock->info();
            showOptions();
            break;
        case 9:
            showOptions();
            break;
        case 0:
            delete stock;

            break;
        default:
            cout << "There's no such an option!\n";
            showOptions();
            break;
        }
    }
}