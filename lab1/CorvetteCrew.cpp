#include <iostream>

#include "CorvetteCrew.h"

using std::cout;
using std::endl;

void CorvetteCrew::info() const {
    cout << "Corvette crew #" << _number << endl;
}