#pragma once

#include "DestroyerAttribute.h" 

// Конкретный класс, корпус эсминца
class DestroyerHull : public DestroyerAttribute {
public:
    DestroyerHull(int _number) : DestroyerAttribute(_number) {};

    void info() const override;
};