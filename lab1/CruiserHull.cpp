#include <iostream>

#include "CruiserHull.h"

using std::cout;
using std::endl;

void CruiserHull::info() const {
    cout << "Cruiser hull #" << _number << endl;
}