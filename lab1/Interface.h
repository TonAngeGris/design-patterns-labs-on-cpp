#pragma once

class Interface {
private:
	void showOptions() const;

public:
	void run();
};
