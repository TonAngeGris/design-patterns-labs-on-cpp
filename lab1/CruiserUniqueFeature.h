#pragma once

#include "CruiserAttribute.h"

// Конкретный класс, мощность ускорителей эсминца (уникальная особенность)
class CruiserUniqueFeature : public CruiserAttribute {
public:
    CruiserUniqueFeature(int _number) : CruiserAttribute(_number) {};

    void info() const override;
};